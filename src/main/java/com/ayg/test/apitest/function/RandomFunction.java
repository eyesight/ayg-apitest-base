package com.ayg.test.apitest.function;

import com.ayg.test.apitest.utils.RandomUtil;

public class RandomFunction implements Function {

	@Override
	public String execute(String[] args) {
		int len = args.length;
		int length = 6;// 默认为6
		boolean flag = false;// 默认为false
		if (len > 0) {// 第一个参数字符串长度
			length = Integer.valueOf(args[0]);
		}
		if (len > 1) {// 第二个参数是否纯字符串
			flag = Boolean.valueOf(args[1]);
		}
		return RandomUtil.getRandom(length, flag);
	}

	@Override
	public String getReferenceKey() {
		return "random";
	}

	public static void main(String[] args){
		RandomFunction randomFunction = new RandomFunction();
		System.out.println(randomFunction.execute(new String[]{"10","true"}));
	}

}
