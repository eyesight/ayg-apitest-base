package com.ayg.test.apitest.function;

import com.ayg.test.apitest.utils.StringUtil;

public class EnvFunction implements Function{
	private static String testBase = "http://192.168.1.92:11972";
	private static String uatBase = "http://192.168.1.92:11972";
	private static String proBase = "http://192.168.1.92:11972";
	private static String testNotify= "http://192.168.1.90:20000/simulator/api/yjf/busireturn";
	private static String uatNotify= "http://192.168.1.90:20000/simulator/api/yjf/busireturn";
	private static String proNotify= "http://192.168.1.90:20000/simulator/api/yjf/busireturn";
	@Override
	public String execute(String[] args) {
		String env = args[0];
		String url = args.length>1?args[1]:null;
		if ("test".equals(env)) {
			return StringUtil.isNotEmpty(url)?testNotify:testBase;
		} else if ("uat".equals(env)) {
			return StringUtil.isNotEmpty(url)?uatNotify: testBase;
		} else if ("pro".equals(env)) {
			return StringUtil.isNotEmpty(url)?proNotify: testBase;
		}

		return env;
	}

	@Override
	public String getReferenceKey() {
		return "env";
	}

}
