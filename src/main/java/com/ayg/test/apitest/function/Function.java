package com.ayg.test.apitest.function;

public interface Function {
	String execute(String[] args);

	String getReferenceKey();
}
