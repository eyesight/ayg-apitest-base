package com.ayg.test.apitest.function;

import com.ayg.test.apitest.dao.BaseDao;
import org.apache.commons.collections.ListUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbSelectFunction implements Function {
    @Override
    public String execute(String[] args) {
        BaseDao dao = new BaseDao();
        String env = args[0];
        String sql = args[1];
        List result = dao.query(env,sql);
        if(result==null || result.size()==0){
            return "";
        }
        Object[] objarray = (Object[])result.get(0);
        if(objarray==null || objarray.length==0){
            return "";
        }
        if(objarray[0] instanceof BigDecimal){
            return objarray[0].toString();
        }else if(objarray[0] instanceof Integer){
            return String.valueOf(objarray[0]);
        }else if(objarray[0] instanceof Float){
            return String.valueOf(objarray[0]);
        }else if(objarray[0] instanceof Double){
            return String.valueOf(objarray[0]);
        }else if(objarray[0] instanceof Timestamp){
            return objarray[0].toString();
        }else {
            return (String) objarray[0];
        }
    }

    @Override
    public String getReferenceKey() {
        return "database";
    }

    public static void main(String[] args) {
        DbSelectFunction dbSelectFunction = new DbSelectFunction();
        String[] funcArgs = new String[]{"test","select trade_no from ayg_newpayment.payroll_tradeno where trade_no > '191119015303264' limit 10"};
        String returnValue = dbSelectFunction.execute(funcArgs);
        System.out.println(returnValue);
    }





}
