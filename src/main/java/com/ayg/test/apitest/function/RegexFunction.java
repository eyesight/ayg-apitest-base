package com.ayg.test.apitest.function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFunction implements Function {
    @Override
    public String execute(String[] args) {
        String rule = args[0];
        Pattern pattern = Pattern.compile(rule);
        Matcher m = pattern.matcher(args[1]);
        while (m.find()) {
           return "true";
        }
        return "false";
    }

    @Override
    public String getReferenceKey() {
        return "regex";
    }

}
