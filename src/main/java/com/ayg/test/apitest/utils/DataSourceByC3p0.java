package com.ayg.test.apitest.utils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;

import javax.sql.DataSource;

public class DataSourceByC3p0 {

    private static DataSource dataSource;
    static {
        //System.setProperty("com.mchange.v2.c3p0.cfg.xml","config/c3p0-config.xml");
        dataSource = new ComboPooledDataSource();
    }

    public static QueryRunner getQueryRunner(){
        return new QueryRunner(dataSource);
    }

    public static QueryRunner getQueryRunner(String env){
        dataSource = new ComboPooledDataSource(env);
        return new QueryRunner(dataSource);
    }

}
