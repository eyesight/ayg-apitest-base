package com.ayg.test.apitest.utils;

import com.ayg.test.apitest.entity.Validate;
import com.csvreader.CsvReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class CsvUtil {
    public static List readCSV(String csvFilePath) {
        List paramList = new ArrayList();
        CsvReader reader = null;
        try {
            // 用来保存数据
            ArrayList<String[]> csvFileList = new ArrayList<String[]>();
            // 创建CSV读对象 例如:CsvReader(文件路径，分隔符，编码格式);
            reader = new CsvReader(CsvUtil.class.getResource(csvFilePath).getPath(), ',', Charset.forName("UTF-8"));
            // 跳过表头 如果需要表头的话，这句可以忽略
            reader.readHeaders();
            // 逐行读入除表头的数据
            String[] headers = reader.getHeaders();
            while (reader.readRecord()) {
                csvFileList.add(reader.getValues());
            }
            // 遍历读取的CSV文件
            for (int row = 0; row < csvFileList.size(); row++) {
                Map paramMap = new HashMap();
                // 取得第row行第0列的数据
                for(int i=0; i< headers.length;i++){
                    String cell = csvFileList.get(row)[i];
                    paramMap.put(headers[i],cell);
                }
                paramList.add(paramMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (reader != null) {
                reader.close();
            }
        }
        return paramList;
    }

    public static Map<Integer,List> readValidateDataFile(String filePath) {
        Map<Integer,List> dataMap = new HashMap<Integer, List>();
        CsvReader reader = null;
        ArrayList<String[]> csvFileList = new ArrayList<String[]>();
        try {
            reader = new CsvReader(CsvUtil.class.getResource(filePath).getPath(), ',', Charset.forName("UTF-8"));
            //reader = new CsvReader(filePath, ',', Charset.forName("UTF-8"));
            if (reader != null) {
                reader.readHeaders();
                String[] headers = reader.getHeaders();
                while (reader.readRecord()) {
                    csvFileList.add(reader.getValues());
                }
                reader.close();
                List<Validate> validates = new ArrayList<Validate>();
                Integer index =  null;
                // 遍历读取的CSV文件
                for (int row = 0; row < csvFileList.size(); row++) {
                    Validate validate  = new Validate();
                    // 取得第row行第0列的数据
                    for(int i=0; i< headers.length;i++){
                        String cell = csvFileList.get(row)[i];
                        if("index".equals(headers[i])){
                            index = Integer.parseInt(cell);
                            if(dataMap.get(index)!=null){
                               validates = dataMap.get(index);
                            }else {
                                validates = new ArrayList<Validate>();
                                dataMap.put(index,validates);
                            }
                        }else  if("check".equals(headers[i])){
                            validate.setCheck(cell);
                        }else if("comparator".equals(headers[i])){
                            validate.setComparator(cell);
                        }else if("expect".equals(headers[i])){
                            validate.setExpect(cell);
                        }
                    }
                    validates.add(validate);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return dataMap;
    }

    public static void main(String[] args){
        Map<Integer,List> dataMap = readValidateDataFile("E:\\jenny\\ideaprojects\\api-test-base\\src\\test\\resources\\env\\test\\dlvopenapi\\query-balance-detail-validate.csv");
        int index = 0;
        List list = dataMap.get(index);
        System.out.println();
    }

}
