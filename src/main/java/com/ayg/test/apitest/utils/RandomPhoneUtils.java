package com.ayg.test.apitest.utils;

public class RandomPhoneUtils {

    public static int getNum(int start,int end) {
        return (int)(Math.random()*(end-start+1)+start);
    }

    /**
     * 返回手机号码
     */
    private static String[] telFirst="134,135,136,137,138,139,150,151,152,157,158,159,130,131,132,155,156,133,153".split(",");

    //type [0：随机，1:奇数结尾，2：偶数结尾]
    private static String getTel(int type) {
        int index=getNum(0,telFirst.length-1);
        String first=telFirst[index];
        String second=String.valueOf(getNum(1,888)+10000).substring(1);
        int thirdInt = getNum(1,9100);
        String third;
        switch (type){
            case 1: thirdInt=isOdd(thirdInt)?  thirdInt: thirdInt +1;break;
            case 2: thirdInt=isOdd(thirdInt)? thirdInt +1: thirdInt;break;
            default:break;
        }
        third=String.valueOf(thirdInt+10000).substring(1);
        return first+second+third;
    }


    private static boolean isOdd(int num){
        if(num%2 != 0){   //是奇数
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        for (int i = 0; i < 700; i++) {
            System.out.println(getTel(2));
        }
    }

}
