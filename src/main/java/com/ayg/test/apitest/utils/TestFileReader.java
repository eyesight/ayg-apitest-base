package com.ayg.test.apitest.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TestFileReader {
    private static final Logger logger = LoggerFactory.getLogger(TestFileReader.class);
    public static List<String> readfile(String filepath) {
        List<String> files = new ArrayList<>();
        filepath = TestFileReader.class.getResource(filepath).getPath();
        //filepath = System.getProperty("user.dir")+filepath;//jar包外读取用例文件
        File file = new File(filepath);
        String[] filelist = file.list();
        if (filelist != null) {
            for (int i = 0; i < filelist.length; i++) {
               // File readfile = new File(filepath + "\\" + filelist[i]);
                File readfile = new File(filepath + File.separator + filelist[i]);
                if (!readfile.isDirectory()) {
                    //TODO:是否考虑递归目录的读取
                    files.add(filepath + File.separator + readfile.getName());
                }
            }
        }
        return files;
    }

    public static String readFileContent(String path) {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {
            String s = "";
            StringBuilder tmpstr = new StringBuilder();
            while ((s = br.readLine()) != null) {
                tmpstr.append(s);
            }
            return tmpstr.toString();
        } catch (IOException e) {
            logger.error("content:{}",e);
        }
        return null;
    }
}
