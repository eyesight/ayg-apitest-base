package com.ayg.test.apitest.utils;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

public class YamlRead {

    String path = "";
    public YamlRead(){

    }
    public YamlRead(String path) {
        this.path = path;
    }

    public Object getYmal() throws YamlException, FileNotFoundException {
       // String path = getClass().getResource(this.path).getPath();
        YamlReader reader = new YamlReader(new FileReader(path));
        Object object = reader.read();
        return  object;
    }

    public void writeYaml(List list,String ymlFileName) throws Exception{
        YamlWriter writer = new YamlWriter(new FileWriter(ymlFileName));
        writer.write(list);
        writer.close();
    }



}
