package com.ayg.test.apitest.core.asserts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Maps;

import java.util.Map;

public class DatabaseAssert extends Assertion {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseAssert.class);
    private Map<AssertionError, IAssert> mErrors = Maps.newLinkedHashMap();
    @Override
    public void executeAssert(IAssert a) {
        try {
            a.doAssert();

        } catch(AssertionError ex) {
            onAssertFailure(a);
            mErrors.put(ex,a);
            logger.info("写数据库");
        }
    }
    /**
     * 结算
     */
    public void assertAll() {
        if (! mErrors.isEmpty()) {
            StringBuilder sb = new StringBuilder("The following asserts failed:\n");
            boolean first = true;
            for (Map.Entry<AssertionError, IAssert> ae : mErrors.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(ae.getKey().getMessage());
            }
            throw new AssertionError(sb.toString());
        }
    }
}
