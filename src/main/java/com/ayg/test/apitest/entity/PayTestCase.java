package com.ayg.test.apitest.entity;

import java.util.Date;

public class PayTestCase {
    private int id;
    private String name;
    private String methond;
    private String url;
    private String request;
    private String validate;
    private String req_result;
    private String pay_result;
    private int status;
    private String api_msg;
    private String env;
    private int type;
    private int case_type;

    public String getApi_msg() {
        return api_msg;
    }

    public void setApi_msg(String api_msg) {
        this.api_msg = api_msg;
    }

    private Date create_time;
    private Date update_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethond() {
        return methond;
    }

    public void setMethond(String methond) {
        this.methond = methond;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReq_result() {
        return req_result;
    }

    public void setReq_result(String req_result) {
        this.req_result = req_result;
    }

    public String getPay_result() {
        return pay_result;
    }

    public void setPay_result(String pay_result) {
        this.pay_result = pay_result;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCase_type() {
        return case_type;
    }

    public void setCase_type(int case_type) {
        this.case_type = case_type;
    }
}
