package com.ayg.test.apitest.constants;

public class SqlString {
    public static final String PAYMENT_TESTCASE_SQL =
            "select * " +
            " from ayg_api_test.pay_testcase " +
            " where status in (0,1) and env=? ";

    //支付接口处理结果sql
    public static final String PAYMENT_TESTCASE_STATE_SQL=
            "SELECT a.trade_no,a.request_id,a.state,a.notify_resultcode ,b.deal_resultmessage\n" +
            " FROM ayg_newpayment.payroll_tradeno a,ayg_newpayment.payroll_tradeno_order_detail b\n" +
            " WHERE a.request_id=?\n" +
            " AND a.trade_no = b.trade_no";
    public static final String UPDATE_TESTCASE_STATUS = "update ayg_api_test.pay_testcase set status = ? where id=?";

    public static final String INSERT_PAYMENT_TESTCASE = "insert into " +
            " ayg_api_test.pay_testcase(name,request,validate,req_result,status,api_msg,env,type) " +
            " values(?,?,?,?,?,?,?,?)";

    //调帐接口处理结果sql
    public static final String ADJUST_TESTCASE_STATE_SQL =
            "SELECT adjust_state,notify_resultcode,adjust_resultmessage\n" +
            " FROM ayg_newpayment.adjust_tradeno \n" +
            " WHERE request_id=?";

    public static final String UPDATE_PAYMENT_TESTCASE_STATUS_SQL =  "UPDATE  " +
            "  ayg_api_test.pay_testcase  SET status = ? " +
            " where id = ? ";
}
