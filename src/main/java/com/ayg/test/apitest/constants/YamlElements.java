package com.ayg.test.apitest.constants;

public class YamlElements {
    public static final String CONFIG = "config";
    public static final String REQUEST = "request";
    public static final String BASE_URL = "base_url";
    public static final String HEADERS = "headers";
    public static final String COOKIES = "cookies";
    public static final String JSON = "json";
    public static final String VARIABLES = "variables";
    public static final String TEST = "test";
    public static final String NAME = "name";
    public static final String METHOD = "method";
    public static final String URL = "url";
    public static final String EXTRACT = "extract";
    public static final String VALIDATE = "validate";
    public static final String COMPARATOR = "comparator";
    public static final String CHECK = "check";
    public static final String EXPECT = "expect";
    public static final String SQL = "sql";
    public static final String SIGN = "sign";
    public static final String TYPE = "type";
    public static final String COMMON = "common";
    public static final String PARAM_TYPE = "paramType";
}
