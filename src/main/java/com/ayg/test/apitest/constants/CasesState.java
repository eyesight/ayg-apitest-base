package com.ayg.test.apitest.constants;

public class CasesState {
    private CasesState(){}
    public static final int DEFAULT = -1;//初始状态
    public static final int DEAL_FAIL = 0;//支付交易受理失败
    public static final int DEAL_SUCCESS = 1;//支付交易受理成功
    public static final int TRADE_PROCESSING = 2;//支付交易处理中
    public static final int SUCCESS = 3;//支付交易处理成功&&通知业务方成功
    public static final int FAIL = 4;//支付交易处理失败或者通知业务方失败
}
