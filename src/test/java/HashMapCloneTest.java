import java.util.HashMap;

public class HashMapCloneTest {
    public static void main(String[] args){
        HashMap hm_source = new HashMap();
        HashMap hm_clone = new HashMap();
        hm_source.put("1", "1");

// hashmap deep clone method 1
     //   hm_clone = (HashMap)hm_source.clone();
// hashmap deep clone method 2
       hm_clone.putAll(hm_source);// hashmap shadow clone
// hm_b = hm_a;

        hm_source.put("2", "2");
        hm_clone.put("3","3");
        System.out.println("hm_source增加元素后，hm_source:"+hm_source);
        System.out.println("hm_source增加元素后，hm_clone:"+hm_clone);

        System.out.println("是否指向同一内存地址:"+(hm_source==hm_clone));
        System.out.println("第一个元素是否指向同一内存地址:"+(hm_source.get(1)==hm_clone.get(1)));
    }
}
